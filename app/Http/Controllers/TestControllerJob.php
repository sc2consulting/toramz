<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\JobTest;
use App\Http\Controllers\Controller;

class TestControllerJob extends Controller
{
    /**
     * Store a new podcast.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Create podcast...

        JobTest::dispatchNow($podcast)->onQueue('tube' . rand(1,3));
    }
}
